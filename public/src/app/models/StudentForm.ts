export class StudentForm {

    constructor(
        public studentClass: string,
        public year: number,
        public quarter: string,
        public studentName?: string,
        public dob?: string,
        public mathematicsGrades?: number,
        public literatureGrades?: number,
        public computerGrades?: number,
    ) {  }

}
