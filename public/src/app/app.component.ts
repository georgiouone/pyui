import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Student} from "./models/Student";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  graphData: JSON;
  studentsData: JSON;
  quarterData: null;
  resp: null;
  searchText : string;
  data : JSON;
  studentData : Student;
  subjectList = ['Math', 'Computer', 'Literature'];

  constructor(private httpClient: HttpClient) { 
  }

  ngOnInit() {
      this.getAllStudents();
      this.getQuarterList();
  }

  onSubmit() {
      this.httpClient.post('http://127.0.0.1:8080/student', JSON.stringify(this.data)).subscribe(data => {
          console.log(this.studentsData);
          this.studentsData = data as JSON;
      })
  };


  getStudentById = function(studentId) {
    console.log('studentId', studentId);
        this.httpClient.get('http://127.0.0.1:8080/student/' + studentId).subscribe(response => {
            this.studentData = response.data as JSON;
        })
    };

  statsStudentPerQuarter = function(studentId) {
        this.httpClient.get('http://127.0.0.1:8080/statistics/studentperquarter/' + studentId).subscribe(data => {
            this.graphData = data as JSON;
        })
    };

  statsStudentSubject = function(subject) {
        this.httpClient.get('http://127.0.0.1:8080/statistics/studentpersubject/' + subject).subscribe(data => {
            this.graphData = data as JSON;
        })
  };

    statsQuarterPerSubject = function(year, quarter) {
        this.httpClient.get('http://127.0.0.1:8080/statistics/quarter/student/' + year + '/' + quarter).subscribe(data => {
            this.graphData = data as JSON;
            console.log(this.graphData);
        })
    };

  getQuarterList = function() {
        this.httpClient.get('http://127.0.0.1:8080/quarter/list').subscribe(response => {
            this.quarterData = response.data as JSON;
            console.log(this.quarterData);
        })
  };

  getAllStudents = function() {
    this.httpClient.get('http://127.0.0.1:8080/students/list').subscribe(data => {
      this.studentsData = data as JSON;
    })
  }

}
