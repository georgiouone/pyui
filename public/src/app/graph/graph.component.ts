import {Component, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as CanvasJS from '../../../lib/canvasjs.min.js';

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit, OnChanges {
  @Input() canvas:any;

    ngOnChanges(changes: SimpleChanges){
        if (changes['canvas']){
            if (this.canvas){
                let chart = new CanvasJS.Chart("chartContainer", {
                    animationEnabled: true,
                    exportEnabled: true,
                    title: {
                        text: this.canvas.title
                    },
                    data: [{
                        type: "column",
                        dataPoints: this.canvas.data
                    }]
                });
                chart.render();
            }
        }
    }


  constructor(private httpClient: HttpClient) {
  }

  ngOnInit() {
  }


}
