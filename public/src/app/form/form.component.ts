import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StudentForm} from "../models/StudentForm";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

    response: any;

    constructor(private httpClient: HttpClient) {
    }

    ngOnInit() {
    }

    //initialize
    classList = ['IT Class', 'Math Class', 'Literature Class'];
    quarterList = ['Q1', 'Q2', 'Q3', 'Q4'];
    model = new StudentForm(this.classList[0], 2018, this.quarterList[0]);

    submitForm() {
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        this.httpClient.post('http://127.0.0.1:8080/student', JSON.stringify(this.model), {headers: headers}).subscribe(data => {
	    this.model = new StudentForm(this.classList[0], 2018, this.quarterList[0]);
            this.response = data;
            console.log('submited', data);
        })
    }

}
