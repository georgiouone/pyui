import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], searchText: string, property: string): any[] {
        if (!items) return [];
        if (searchText === 'All' || searchText == null || searchText== '') return items;

        return items.filter(it => {
// console.log(property);
//debugger
            if (property && it && searchText) {
                return it[ property ].toLowerCase().includes(searchText.toLowerCase());
            }
            if (typeof(it) != 'undefined' && it && searchText) {
                return it.toLowerCase().includes(searchText.toLowerCase());
            }
        });

    }
}