
FRONTEND is Angular
	
	* is stored under `public` folder
	* Make sure you have all depencies install by running `npm i`
	* including angular: npm install -g @angular/cli
	* run with `ng serve`
	* access URL is http://localhost:4200


BACKEND is python (bottlepy)

	* install dependencies RUN `pip install -r requirements.txt `
	* create your database and load the database schema from db folder
	* make sure you have the correct configuration to connect on db under foler lib/
	* run `python server.py`

BUGs

	* BACKEND do not have any validation
	* FRONTEND do not have validation submit form yet