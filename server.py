import bottle_mysql
import simplejson as json

from bottle import Bottle, route, run, debug, template, static_file, error, request, response, HTTPError, get, post

app = Bottle()

app.config.load_config('lib/settings.conf');

dbuser = app.config['mysql.username']
dbpass = app.config['mysql.password']
dbname = app.config['mysql.database']
reloader = app.config['server_settings.reloader']
debug = app.config['server_settings.debug']

# dbhost is optional, default is localhost
plugin = bottle_mysql.Plugin(dbuser=dbuser, dbpass=dbpass, dbname=dbname)
app.install(plugin)


@app.hook('after_request')
def enable_cors():
    """
    You need to add some headers to each request.
    Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
    """
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token, Authorization'

#get all students
@app.route('/students/list', method='GET')
def getAllStudents(db):
    db.execute('SELECT * from students')
    data = db.fetchall()
    response.headers['Content-Type'] = 'application/json'
    return json.dumps(data)


# show specific student
@app.route('/student/<id:int>', method='GET')
def getStudentById(id, db):

    db.execute("select * from students where id = %s;" % (id))
    data = db.fetchone()

    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'data' : data, 'query' : {'id' : id}})


# create student
@app.route('/student', method='OPTIONS')
def createStudent():
    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'data' : 'dasdsadadas'})


@app.route('/student', method='POST')
def createStudent(db):
    try:
        data = request.json
        studentName = data['studentName']
        dob = data['dob']
        studentClass = data['studentClass']
        year = data['year']
        quarter = data['quarter']
        mathematics = data['mathematicsGrades']
        computer_science = data['computerGrades']
        literature = data['literatureGrades']

        db.execute("select id from students where name = %s AND birthday = %s;", [studentName, dob])
        studentExists = db.fetchone()

        rspMsg = '';

        if studentExists:
          studentId = studentExists['id']
          rspMsg += "New grades has been added for " + studentName;
        else:
          db.execute("INSERT INTO `students` (`name`, `birthday`, `class`) VALUES (%s, %s, %s)", [studentName, dob, studentClass])
          studentId = db.lastrowid
          rspMsg += "New Student (" + studentName + ") has been added including the grades";


        db.execute("INSERT INTO quarter_grades (`student_id`, `year`, `quarter`, `math_grade`, `computer_grade`, `literature_grade`) VALUES (%s, %s, %s, %s, %s, %s)", [studentId, year, quarter, mathematics, computer_science, literature])
        studentRecord = db.lastrowid

        if studentRecord:
          rspMsg += " of quarter " + str(quarter) + " and year " + str(year)
        else:
          raise HTTPError(500, "Record did not insert")

        response.headers['Content-Type'] = 'application/json'
        return json.dumps({'success' : rspMsg})
    except Exception as e:
        return json.dumps({'error' : e.message})

#show student quarter
@app.route('/statistics/studentperquarter/<studentId:int>', method='GET')
def statsStudentQarter(studentId, db):
    db.execute("select year, quarter, AVG(math_grade + computer_grade + literature_grade) as average from quarter_grades where student_id = %s group by year, quarter;", [studentId])
    data = db.fetchall()

    db.execute("select name from students where id = %s;", [studentId])
    student = db.fetchone()

    graph = []
    for l in data:
      label = str(l['year']) + ' - ' + l['quarter']
      y = l['average']
      graph.append({'y' : y, 'label' : label})

    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'data' : graph, 'title' : student['name'] + "'s Average"})


@app.route('/statistics/studentpersubject/<subject>', method='GET')
def statsStudentPerSubject(subject, db):

    columnName = subject + '_grade'
    db.execute("select year, quarter, AVG(%s) as %s FROM quarter_grades group by year, quarter;" % (columnName, subject))
    data = db.fetchall()

    graph = []
    for l in data:
      label = str(l['year']) + ' - ' + l['quarter']
      y = l[subject]
      graph.append({'y' : y, 'label' : label})

    title = columnName + ' Average'
    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'data' : graph, 'title' : title})


@app.route('/statistics/quarter', method='GET')
def statsQuarter(db):

    db.execute("select year, quarter, AVG(math_grade + computer_grade + literature_grade) FROM quarter_grades group by year, quarter;")
    data = db.fetchall()

    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'data' : data})


@app.route('/statistics/quarter/student/<year:int>/<quarter>', method='GET')
def statsQuarterOfStudent(year, quarter, db):

    db.execute("select AVG(math_grade) as avarage FROM quarter_grades where year = %s AND quarter = %s;", [year, quarter])
    maths = db.fetchone()

    db.execute("select AVG(computer_grade) as avarage FROM quarter_grades where year = %s AND quarter = %s;", [year, quarter])
    computer = db.fetchone()

    db.execute("select AVG(literature_grade) as avarage FROM quarter_grades where year = %s AND quarter = %s;", [year, quarter])
    literature = db.fetchone()

    graph = []
    graph.append({'y' : maths['avarage'], 'label' : 'maths'})
    graph.append({'y' : computer['avarage'], 'label' : 'computer'})
    graph.append({'y' : literature['avarage'], 'label' : 'literature'})

    label = str(year) + ' - ' + str(quarter)

    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'data' : graph, 'title' : 'Quarter' + label})


@app.route('/quarter/list', method='GET')
def getAllQuarters(db):

    db.execute("select year, quarter FROM quarter_grades group by year, quarter;")
    data = db.fetchall()

    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'data' : data})


@error(403)
def mistake403(code):
    return json.dumps({'data' : "Invalid URL", 'code' : 403})


@error(404)
def mistake404(code):
    return json.dumps({'data' : "URL not found", 'code' : 404})


run(app, host='localhost', port=8080, reloader=reloader, debug=debug)